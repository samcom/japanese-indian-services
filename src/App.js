import React from "react";
import {Routes, Route } from "react-router-dom";
import Footer from "./component/Footer";
import Navbar from "./component/Navbar";
import ScrollToTop from "./component/ScrollToTop";
import {
  Home,
  Overview,
  BusinessPlan,
  Facility,
  ResearchDevelopement,
  Employment,
  Contact,
} from "./screens";

const App = () => {
  return (
    <>
      <ScrollToTop>
        <Navbar />
        <Routes>
          <Route path="/" element={<Home />} />

          <Route path="/overview" element={<Overview />} />
          <Route path="/businessPlan" element={<BusinessPlan />} />
          <Route path="/facility" element={<Facility />} />
          <Route
            path="/researchDevelopement"
            element={<ResearchDevelopement />}
          />
          <Route path="/employment" element={<Employment />} />
          <Route path="/contact" element={<Contact />} />
        </Routes>
        <Footer />
      </ScrollToTop>
    </>
  );
};

export default App;
