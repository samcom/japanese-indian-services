import React from "react";
import FacilityHead from "../component/Facility/FacilityHead";
import FacilityCard from "../component/Facility/FacilityCard";

const Facility = () => {
  return (
    <>
      <FacilityHead />
      <FacilityCard />
    </>
  );
};

export default Facility;
