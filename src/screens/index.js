import Home from "./Home";
import Overview from "./Overview";
import BusinessPlan from "./BusinessPlan";
import Facility from "./Facility";
import ResearchDevelopement from "./ResearchDevelopement";
import Employment from "./Employment";
import Contact from "./Contact";

export {
  Home,
  Overview,
  BusinessPlan,
  Facility,
  ResearchDevelopement,
  Employment,
  Contact,
};
