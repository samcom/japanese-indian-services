import React from "react";
import ComingSoon from "../component/Employment/ComingSoon";
import EmploymentHead from "../component/Employment/EmploymentHead";

const Employment = () => {
  return (
    <>
      <EmploymentHead />
      <ComingSoon />
    </>
  );
};

export default Employment;
