import React from "react";
import BusinessPlanHead from "../component/BusinessPlan/BusinessPlanHead";
import MainAttraction from "../component/BusinessPlan/MainAttraction";
import MarketOutloook from "../component/BusinessPlan/MarketOutlook";
import ProjectImplementation from "../component/BusinessPlan/ProjectImplementation";
import BusinessCard from "../component/BusinessPlan/BusinessCard";
import BusinessExp from "../component/BusinessPlan/BusinessExp";

const BusinessPlan = () => {
  return (
    <>
      <BusinessPlanHead />
      <BusinessCard />
      <BusinessExp />
      <ProjectImplementation />
      <MainAttraction />
      <MarketOutloook />
    </>
  );
};

export default BusinessPlan;
