import React from "react";
import ResearchHead from "../component/ResearchDevelopement/ResearchHead";
import ResearchData from "../component/ResearchDevelopement/ResearchData";
import ResearchCharge from "../component/ResearchDevelopement/ResearchCharge";

const ResearchDevelopement = () => {
  return (
    <>
      <ResearchHead />
      <ResearchData />
      <ResearchCharge />
    </>
  );
};

export default ResearchDevelopement;
