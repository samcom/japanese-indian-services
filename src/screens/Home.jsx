import React from "react";
import HomeCard from "../component/Home/HomeCard";
import HomeHead from "../component/Home/HomeHead";

const Home = () => {
  return (
    <>
      <HomeHead />
      <HomeCard />
    </>
  );
};

export default Home;
