import React from "react";
import OverviewHead from "../component/Overview/OverviewHead";
import OverviewDetails from "../component/Overview/OverviewDetails"

const Overview = () => {
  return (
    <div>
      <OverviewHead />
      <OverviewDetails/>
    </div>
  );
};

export default Overview;
