import React from "react";
import { BsFacebook, BsTwitter, BsInstagram , BsYoutube } from "react-icons/bs";

const Footer = () => {
  return (
    <>
      <div className="flex flex-col justify-center items-center bg-accent mt-20 py-20">
        <p className="text-sm lg:w-1/3 w-full p-5  text-white text-justify">
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book.
        </p>
        <div className="my-10 flex gap-5">
          <div className="w-10 h-10 bg-white p-2 rounded-full">
            <BsFacebook className="text-accent text-2xl" />
          </div>
          <div className="w-10 h-10 bg-white p-2 rounded-full">
            <BsTwitter className="text-accent text-2xl" />
          </div>
          <div className="w-10 h-10 bg-white p-2 rounded-full">
            <BsInstagram className="text-accent text-2xl" />
          </div>
          <div className="w-10 h-10 bg-white p-2 rounded-full">
            <BsYoutube className="text-accent text-2xl" />
          </div>
        </div>
        <p className="text-sm text-white text-center">
          &copy; {new Date().getFullYear} All Rights Reserved. Website Designed
          & Developed by{" "}
          <a
            href="https://www.samcomtechnologies.com/"
            target="_blank"
            rel="noreferrer"
            className="font-bold"
          >
            Samcom Technologies
          </a>
        </p>
      </div>
    </>
  );
};

export default Footer;
