import React from "react";

const OverviewHead = () => {
  return (
    <>
      <div className="flex justify-center items-center bg-accent h-96 text-white">
        <p className="text-5xl font-medium uppercase mt-20 ">
          Overview
        </p>
      </div>
    </>
  );
};

export default OverviewHead;
