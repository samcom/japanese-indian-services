import React from "react";
import { GiBattery75 } from "react-icons/gi";
import { CgArrowRightR } from "react-icons/cg";
import box from "../../assets/Vector/o_106.png";
import img from "../../assets/Vector/overviewimg.png";
import img2 from "../../assets/Vector/overviewimg2.png";
import Machine from "../../assets/Vector/o_228.png";

const OverviewDetails = () => {
  return (
    <>
      <div className="flex lg:flex-row flex-col mt-10 justify-end">
        <div className="flex lg:justify-end lg:w-2/3 lg:mr-20 w-full justify-center">
          <div className="lg:w-2/3 w-11/12">
            <GiBattery75 className="h-12 w-12 p-2 text-white rounded-full bg-accent" />
            <p className="text-accent font-bold mt-5 text-3xl">
              Environmental, Social, And Governance Risk Briefing
            </p>
            <div className="my-5">
              <div className="grid grid-flow-col items-center">
                <CgArrowRightR className="text-accent " />
                <p className="text-accent text-justify ml-2">
                  Electric mobility is being incentivized around the world to
                  reduce the environmental impact of the transportation sector
                  and to further the transition into a low carbon economy.
                </p>
              </div>
              <div className="grid grid-flow-col items-center mt-2">
                <CgArrowRightR className=" text-accent" />
                <p className="text-accent  text-justify ml-2 text-sm">
                  Electric mobility is being incentivized around the world to
                  reduce the environmental impact of the transportation sector
                  and to further the transition into a low carbon economy.
                </p>
              </div>
            </div>
            <p className="text-accent font-bold text-justify mt-2 text-sm">
              The journey to achieve the goal of the Paris Agreement, limiting
              global warming to 1.5 °C, has opened the way for technological
              innovation to help reach a peak of accenthouse gas (GHG)
              emissions. Technology aims to have the double purpose of improving
              our resilience towards climate change and to reduce GHG emissions.
              That is why technological innovation is key to move towards a low
              carbon economy, where the output of GHGs into the atmosphere is
              minimal.
            </p>
            <p className="font-bold text-accent text-justify mt-2 text-sm">
              Electric mobility is one of the solutions being incentivized
              around the globe in varying magnitudes. Nevertheless, its
              implementation presents challenges to keep up with the growing
              demand of electric batteries and the risks in the lithium supply
              chain. A sustainable approach is needed to address the challenges
              from the extraction of this critical raw material, which is
              crucial in the fight against climate change.
            </p>
          </div>
        </div>
        <div className="flex justify-end items-center relative lg:w-1/3 w-full">
          <img src={Machine} className="lg:absolute right-0 bottom-0 lg:m-0 mt-5 lg:w-full w-11/12" alt="" />
        </div>
      </div>

      <div className="flex w-full gap-10 mt-20 lg:flex-row flex-col">
        <div className="flex justify-center items-center gap-5 lg:w-1/3 order-2">
          <div className=" ml-5">
            <img
              src={img}
              alt=""
              className="p-2 border-accent border-4 rounded-tl-[50px]"
            />
            <img src={box} className="mt-3" alt="" />
          </div>
          <div>
            <img src={box} className="mb-3" alt="" />
            <img
              src={img2}
              alt=""
              className="p-2 border-accent border-4 rounded-br-[50px]"
            />
          </div>
        </div>
        <div className="flex flex-col justify-start lg:w-1/2 w-full p-5">
          <p className=" text-accent font-bold text-right text-4xl my-5">
            The Future Of Mobility
          </p>
          <div className="flex gap-10">
            <p className="text-accent text-sm text-justify">
              With more EVs on the market and strong incentives to increase this
              number, the demand for materials used in EV batteries, such as
              lithium, cobalt, nickel, and others, can be expected to increase
              accordingly. The lithium-ion (Li-ion) batteries are the most
              widely used today and will likely dominate the market in the
              upcoming decade. Compared to its competitors, Li-ion batteries can
              store higher amounts of energy with lower mass and weight, have
              high energy efficiency, good high temperature performance, and
              lower self-discharge. The US Geological Service (USGS) reports
              that in 2020, 71% of the global end use of lithium was for
              batteries. The IEA estimated that the lithium demand could
              increase up to 185,000 tons per year by 2030, compared to the
              17,000 tons per year in 2019, without considering other lithium
              applications like electronic devices, grid storage, ceramics, and
              glass.
            </p>
            <p className="text-accent text-justify text-sm ">
              Within the transportation sector, road transport has by far the
              biggest share in the sector’s GHG emissions. Therefore, one of the
              initiatives to lower them is to incentivize the deployment of low
              emission transport alternatives such as electric vehicles (EVs).
              Scientific studies have reached different conclusions about the
              real environmental footprint of EVs and the extent to which they
              can help mitigate climate change. The differences are due to the
              research scope and other factors such as the energy mix of the
              studied location. Nevertheless, the uptake of EVs continues to
              increase year-on-year. According to a report from the
              International Energy Agency (IEA), the global stock of EVs reached
              7.2 million in 2019. This amount makes up only 1% of the total
              global car stock. However, it also represents a 40% year-on-year
              increase from 2018; EVs keep gaining share in the global car
              sales.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default OverviewDetails;
