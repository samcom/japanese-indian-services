import React from "react";
import v1 from "../../assets/Vector/h_2015.png";
import v2 from "../../assets/Vector/h_2019.png";
import v3 from "../../assets/Vector/h_2020.png";
import v4 from "../../assets/Vector/h_2022.png";
import v5 from "../../assets/Vector/h_2024.png";

const HomeCard = () => {
  return (
    <div className="p-5">
      <div className="grid lg:grid-cols-3 grid-cols-1 gap-10 lg:px-20 p-5 -mt-20">


        <div className="bg-accent rounded flex flex-col justify-center items-center text-center p-10">
          <img src={v1} alt="" className="w-20" />
          <p className="mt-10 text-white">
            Japanese Indian Services Inc was established.
          </p>
        </div>
        <div className="bg-accent rounded flex flex-col justify-center items-center text-center p-10">
          <img src={v2} alt="" className="w-20" />
          <p className="mt-10 text-white">
            Started investigating Lithium Ion battery business.
          </p>
        </div>
        <div className="bg-accent rounded flex flex-col justify-center items-center text-center p-10">
          <img src={v3} alt="" className="w-20" />
          <p className="mt-10 text-white">
            Initiated Lithium Ion battery manufacturing project.
          </p>
        </div>


        <div className="w-full lg:flex hidden justify-center col-span-3 lg:px-40 gap-10 p-5">
          <div className="bg-accent rounded flex flex-col justify-center items-center text-center p-10 w-1/2">
            <img src={v4} alt="" className="w-20" />
            <p className="mt-10 text-white">
              Start building Lithium Ion battery cell manufacturing plant in
              India.
            </p>
          </div>
          <div className="bg-accent rounded flex flex-col justify-center items-center text-center p-10 w-1/2">
            <img src={v5} alt="" className="w-20" />
            <p className="mt-10 text-white">
              Start selling in Indian market and other Asian market.
            </p>
          </div>
        </div>
      </div>
      <div className="w-full flex lg:hidden flex-col mt-10 justify-evenly col-span-3 lg:px-40 gap-10 p-5 pt-0">
        <div className="bg-accent rounded flex flex-col justify-center items-center text-center p-10">
          <img src={v4} alt="" className="w-20" />
          <p className="mt-10 text-white">
            Start building Lithium Ion battery cell manufacturing plant in
            India.
          </p>
        </div>
        <div className="bg-accent rounded flex flex-col justify-center items-center text-center p-10">
          <img src={v5} alt="" className="w-20" />
          <p className="mt-10 text-white">
            Start selling in Indian market and other Asian market.
          </p>
        </div>
      </div>
    </div>
  );
};

export default HomeCard;
