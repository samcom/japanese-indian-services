import React from "react";
import HomeBG from "../../assets/bg/HomeBg.webp";

const HomeHead = () => {
  return (
    <>
      <img src={HomeBG} className="w-full lg:h-screen object-cover" alt="" style={{minHeight : "50vh"}} />
    </>
  );
};

export default HomeHead;
