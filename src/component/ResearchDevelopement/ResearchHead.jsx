import React from 'react'

const ResearchHead = () => {
  return (
    <div>
      <div className="flex justify-center items-center bg-accent h-96">
        <p className='text-5xl font-medium uppercase mt-20 text-white'>R&D</p>
        </div>
    </div>
  )
}

export default ResearchHead