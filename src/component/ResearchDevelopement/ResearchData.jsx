import React from "react";
import machine from "../../assets/Vector/R_D_Prod.png";
import batteries from "../../assets/Vector/R_D_Batteries.png";
import { AiOutlineDoubleLeft } from "react-icons/ai";
import { BsDot } from "react-icons/bs";

const ResearchData = () => {
  return (
    <>
      <div className="lg:w-11/12 w-full">
        <div className="grid lg:grid-cols-2 grid-cols-1 gap-10">
          <div className="relative">
            <img src={batteries} alt="" className="lg:m-0 mb-32" />
            <div className=" rounded-md absolute bottom-0 p-5 lg:px-20 px-5 bg-accent text-white flex justify-center items-center lg:text-2xl text-xl w-full">
              Battery prototype equipment that can be used
            </div>
          </div>
          <div>
            <div className="p-5 ">
              <p className="text-3xl  text-accent font-bold py-20">
                Development of Future Battery Chemistry
              </p>
              <div className="flex justify-end items-center text-right mb-10 ">
                <p>
                  Battery prototype evaluation equipment for coating liquid
                  (slurry preparation) - continuous coating (desktop coating) -
                  press-cell assembly-charge / discharge characteristic
                  evaluation is also available.
                </p>
                <AiOutlineDoubleLeft className="ml-5 lg:text-5xl text-9xl  text-accent" />
              </div>
              <div className="flex justify-end items-center text-right mb-10">
                <div>
                  <p>
                    supplement: You can try the following three methods for the
                    stirring device for adjusting the coating liquid. Others
                    will be dealt with after consultation.
                  </p>
                  <p className="flex justify-end items-center">
                    Rotational revolution type{" "}
                    <BsDot className="text-3xl  text-accent" />
                  </p>
                  <p className="flex justify-end items-center">
                    Planetary type <BsDot className="text-3xl text-accent" />
                  </p>
                  <p> Thin film high-speed swivel type (Primix Corporation)</p>
                </div>
                <AiOutlineDoubleLeft className="ml-5 text-4xl text-accent" />
              </div>
              <div className="flex justify-end items-center text-right mb-10">
                <p>
                  Our operator will operate and operate each equipment according
                  to your instructions.
                </p>
                <AiOutlineDoubleLeft className="ml-5 text-2xl  text-accent" />
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="flex lg:flex-row flex-col justify-center items-center mt-20">
        <img src={machine} alt="" className="h-72 w-72"></img>
        <div className="bg-accent rounded-md p-10 lg:ml-10 mt-10 lg:mt-0">
          <p className="text-2xl  font-medium text-white">
            Lithium Ion Battery Cell
          </p>
          <ul className="list-disc text-white text-sm mt-10">
            <li className="my-2 ">Electrode manufacturing department</li>
            <li className="my-2 ">Cell manufacturing department</li>
            <li className="my-2 ">Charge/Discharge facility</li>
            <li className="my-2 ">Electrode manufacturing department</li>
            <li className="my-2 ">Lab for research</li>
          </ul>
        </div>
      </div>
    </>
  );
};

export default ResearchData;
