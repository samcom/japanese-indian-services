import React from "react";

const ResearchCharge = () => {
  return (
    <div className="lg:px-32 p-5 mt-20">
      <p className="text-accent text-2xl">Charge/Discharge</p>
      <p className="text-accent text-sm">Prototype Characteristics</p>
      <div className="grid lg:grid-cols-3 grid-cols-1 gap-20 justify-center mt-10">
        <div className="border-2 border-accent p-5 w-full rounded-md ">
          <p className="text-2xl text-accent font-medium">01</p>
          <p className="mt-5 font-medium text-lg">Charge / discharge test</p>
          <p className="w-full bg-accent p-0.5 mt-3"></p>
          <p className="mt-5">
            Evaluate the charge / discharge characteristics of prototype
            batteries and commercially available batteries (initial capacity,
            rate characteristics, input / output characteristics)
          </p>
        </div>
        <div className="border-2 rounded-md border-accent p-5">
          <p className="text-2xl text-accent font-medium">02</p>
          <p className="mt-5 font-medium text-lg">Cycle Life Characteristics</p>
          <p className="w-full bg-accent p-0.5 mt-3"></p>
          <p className="mt-5">
            Durability is evaluated by charge / discharge cycle tests of
            prototype batteries and commercially available batteries. Durability
            evaluation by repeating charging and discharging and changing
            capacity.
          </p>
          <div className="  grid grid-flow-col items-center justify-center mt-5">
            <span className="p-1 bg-accent"></span>
            <p className="ml-2">You can perform cycle tests of laminated laminated cells using the materials supplied by the customer.</p>
          </div>
          <div className="grid grid-flow-col items-center justify-center mt-5">
            <span className="p-1 bg-accent"></span>
            <p className="ml-2">You can perform cycle tests of laminated laminated cells using the materials supplied by the customer.</p>
          </div>
        </div>
        <div className="border-2 rounded-md border-accent p-5">
          <p className="text-2xl text-accent font-medium">01</p>
          <p className="mt-5 font-medium text-lg">AC impedance evaluation</p>
          <p className="w-full bg-accent p-0.5 mt-3"></p>
          <p className="mt-5">AC impedance measurement is performed to evaluate basic battery characteristics and analyze the cause of deterioration.
          </p>
        </div>
      </div>
    </div>
  );
};

export default ResearchCharge;
