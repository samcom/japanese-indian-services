import React from "react";
import Soon from "../../assets/Vector/comingsoon.png";

const ComingSoon = () => {
  return (
    <>
      <div className="flex flex-col justify-center items-center my-20">
        <div className="flex justify-center items-center flex-col">
          <p className="text-accent text-4xl text-left w-1/2">
            coming...
          </p>
          <img src={Soon} alt="" className="w-1/2 mt-5" />

          <div className="flex gap-10 mt-10">
            <div>
              <div className="flex gap-2">
                <div className="text-accent bg-slate-300 rounded">
                  <p className="lg:text-6xl text-4xl font-bold p-2">0</p>
                </div>
                <div className="text-accent bg-slate-300 rounded">
                  <p className="lg:text-6xl text-4xl font-bold p-2">9</p>
                </div>
              </div>
              <p className="tracking-widest text-center">Days</p>
            </div>
            <div>
              <div className="flex gap-2">
                <div className="text-accent bg-slate-300 rounded">
                  <p className="lg:text-6xl text-4xl font-bold p-2">4</p>
                </div>
                <div className="text-accent bg-slate-300 rounded">
                  <p className="lg:text-6xl text-4xl font-bold p-2">5</p>
                </div>
              </div>
              <p className="tracking-widest text-center">Hours</p>
            </div>
            <div>
              <div className="flex gap-2">
                <div className="text-accent bg-slate-300 rounded">
                  <p className="lg:text-6xl text-4xl font-bold p-2">3</p>
                </div>
                <div className="text-accent bg-slate-300 rounded">
                  <p className="lg:text-6xl text-4xl font-bold p-2">0</p>
                </div>
              </div>
              <p className="tracking-widest text-center">Minutes</p>
            </div>
          </div>

          <div className="flex flex-col justify-center items-center">
            <p className="tracking-widest text-3xl font-medium my-10">
              Lorem Ispum Dolor
            </p>
            <p className="w-2/3 text-center">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit,sed do
              eiusmod tempor cididunt ut labore et dolore
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default ComingSoon;
