import React from "react";
import { AiOutlineDoubleRight } from "react-icons/ai";
import board from "../../assets/Vector/B_Group.png";

const BusinessExp = () => {
  return (
    <>
      <div className="grid lg:grid-cols-2 grid-cols-1 justify-center items-center  ">
        <div classname="">
          <p className="lg:px-20 px-5 bg-white text-accent mb-3 text-2xl font-bold ">
            Experience of Project Members
          </p>
          <div className="flex flex-col gap-3 mt-10 lg:px-20 px-5">
            <div className="grid grid-flow-col items-center text-accent ">
              <AiOutlineDoubleRight className="text-accent mr-3" />
              <p classname="">
                Team members have more than 20 years of Experience in Lithium
                ion Battery Cell industry.
              </p>
            </div>
            <div className="grid grid-flow-col items-center justify-start text-accent ">
              <AiOutlineDoubleRight className="text-accent mr-3" />
              <p classname="">
                Have setup 4 Lithium Ion Battery Plant in China.
              </p>
            </div>
            <div className="grid grid-flow-col items-center justify-start text-accent ">
              <AiOutlineDoubleRight className="text-accent mr-3" />
              <p classname="">Have the latest and future battery technology.</p>
            </div>
            <div className="grid grid-flow-col items-center text-accent ">
              <AiOutlineDoubleRight className="text-accent mr-3" />
              <p classname="">
                Japanese Machine will be used for the critical process. We are
                Japanese team, so we are able procure these machines.
              </p>
            </div>
            <div className="grid grid-flow-col items-center text-accent ">
              <AiOutlineDoubleRight className="text-accent mr-3" />
              <p classname="">
                The Company is registered in Japan, so we can participate in
                Japanese University and Government programs, which give us
                unique advantage.
              </p>
            </div>
          </div>
        </div>
        <div className="relative lg:flex lg:justify-center items-center mt-10">
          <img src={board} alt="" className="lg:w-9/12 w-full" />
        </div>
      </div>
    </>
  );
};

export default BusinessExp;
