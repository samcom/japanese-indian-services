import React from "react";

const BusinessPlanHead = () => {
  return (
    <>
      <div className="">
        <div className="flex justify-center items-center bg-accent h-96 text-white">
          <p className="text-5xl font-medium uppercase mt-20 ">Business Plan</p>
        </div>
       
      </div>
    </>
  );
};

export default BusinessPlanHead;
