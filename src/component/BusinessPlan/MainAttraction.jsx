import React from "react";
import Battery from "../../assets/Vector/B_Battery.png";
import { AiOutlineDoubleRight } from "react-icons/ai";

const MainAttraction = () => {
  return (
    <>
      <div className="grid lg:grid-cols-2  grid-cols-1">
        <div className="bg-accent lg:p-10  rounded-md lg:pl-20 p-5">
          <p className="text-white lg:text-3xl text-2xl font-medium">
            Main Attraction of this Project
          </p>
          <div className="text-white mt-10">
            <div className="grid grid-flow-col items-center my-5"> 
              <AiOutlineDoubleRight className="mr-3" />
              <p>
                The India Lithium-Ion Battery market projected to reach USD 7
                billion at a significant CAGR of over 28% during the forecasted
                period of 2020-2027 due to the rise in the adoption of electric
                vehicles across the region.
              </p>
            </div>
            <div className="grid grid-flow-col items-center my-5">
              <AiOutlineDoubleRight className="mr-3" />
              <p>
                The country has an ambitious target of becoming an all-EV nation
                by 2030. In the process toward mass adoption of electric
                vehicles in the country, the government has announced its plan
                to introduce 10,000 electric buses and 50,000 electric rickshaws
                within the next few years. This is expected to raise the
                consumption of lithium-ion batteries.
              </p>
            </div>
            <div className="grid grid-flow-col items-center my-5">
              <AiOutlineDoubleRight className="mr-3" />
              <p>
                The Government of India ordered 10,000 electric cars from Tata
                Motors in September 2017 as part of its plan to increase EV
                adoption in the country. The expected increase in the sale of
                electric vehicles is expected to drive the demand for
                lithium-ion batteries as well.
              </p>
            </div>
            <div className="grid grid-flow-col items-center my-5">
              <AiOutlineDoubleRight className="mr-3" />
              <p>
                Adding to this, in December 2017, the Ministry of Heavy
                Industries and Public Sector Enterprises announced their
                commitment to make public transport fully electric, under the
                Faster Adoption and Manufacturing of (Hybrid and) Electric
                Vehicles (FAME) India scheme.
              </p>
            </div>
            <div className="grid grid-flow-col items-center my-5">
              <AiOutlineDoubleRight className="mr-3" />
              <p>
                Companies such as Exide Industries, Mahindra & Mahindra Limited,
                ACME Cleantech Solutions Private Limited, Reliance Industries
                Limited, NEC India Private Limited, Adani Enterprise Ltd, JSW
                Group, Denso Corp., Samsung SDI Co. Ltd., Rajamane Telectric
                Pvt. Ltd, Suzuki Motor Corp., Bharat Heavy Electricals Ltd., and
                other prominent players are the key players in the India Lithium
                Ion Battery market.
              </p>
            </div>
            <div className="grid grid-flow-col items-center my-5">
              <AiOutlineDoubleRight className="mr-3" />
              <p>
                Companies such as Exide Industries, Mahindra & Mahindra Limited,
                ACME Cleantech Solutions Private Limited, Reliance Industries
                Limited, NEC India Private Limited, Adani Enterprise Ltd, JSW
                Group, Denso Corp., Samsung SDI Co. Ltd., Rajamane Telectric
                Pvt. Ltd, Suzuki Motor Corp., Bharat Heavy Electricals Ltd., and
                other prominent players are the key players in the India Lithium
                Ion Battery market.
              </p>
            </div>
          </div>
        </div>
        <div className="flex justify-center items-center">
          <img src={Battery}  alt="" />
        </div>
      </div>
    </>
  );
};

export default MainAttraction;
