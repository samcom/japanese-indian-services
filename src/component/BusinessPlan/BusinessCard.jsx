import React from "react";
import { IoMdCheckmarkCircle } from "react-icons/io";
import img from "../../assets/Vector/B_Battery1.png";
import { Link } from "react-router-dom";

const BusinessCard = () => {
  return (
    <>
      <div div className="lg:p-20 rounded-md py-10 px-5 mb-8">
        <p className="text-accent text-2xl font-bold mb-2 ">
          Lithium Ion Battery Cell
        </p>
        <p className="text-accent text-sm">
          Chemistry, NMC/Graphite , Capacity
        </p>
        <div className="grid lg:grid-cols-2 grid-cols-1 mt-20 gap-10  ">
          <div className="p-5 bg-accent rounded-md relative">
            <p className="text-white text-sm mb-3">Product Specification</p>
            <div className="grid grid-cols-2 gap-5">
              <div className="text-white">
                <p className="text-3xl font-bold uppercase mb-16 ">item</p>
                <ul>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>Nominal Capacity</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p> Minimum Capacity</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>Nominal voltage</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p> Discharge Cut Off Voltage </p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>Operating Temprraturev</p>
                  </div>
                </ul>
              </div>
              <div className="text-white ">
                <p className="text-3xl font-bold uppercase mb-16 ">spec</p>
                <ul>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>Nominal Capacity</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>39Ah</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>≥ 35Ah</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>3.7V</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>3.0V</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3 " />
                    <p>Charge: 0 ~ 45° C (≤ 1C)</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3 " />
                    <p>Discharge: -20 ~ 0° C ( 1C), 0 ~ 45° C ( 3C)</p>
                  </div>
                </ul>
              </div>
            </div>
            <Link to="/contact">
              <button className="bg-white text-accent p-2 text-xl font-medium absolute bottom-3 right-3 left-3 rounded-md">
                Get Now
              </button>
            </Link>
          </div>
          <div className=" rounded-md p-5 bg-accent flex justify-center items-start flex-col relative">
            <p className="text-white text-sm mb-3">Application:</p>
            <div className="grid grid-cols-3">
              <div className="text-white col-span-2 ">
                <p className="text-3xl font-bold uppercase mb-16 ">
                  SMALL VECHICLES
                </p>
                <ul>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>Dimention</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>Weight</p>
                  </div>
                </ul>
              </div>
              <div className="text-white">
                <p className="text-3xl font-bold uppercase mb-16 ">buses</p>
                <ul>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>Width 118mm X L 342mm</p>
                  </div>
                  <div className="grid grid-flow-col justify-start items-center px-5 my-3">
                    <IoMdCheckmarkCircle className="text-white mr-3" />
                    <p>600 gm</p>
                  </div>
                </ul>
              </div>
            </div>
            <img src={img} alt="" className="w-full mb-20 rounded-md" />
            <Link to="/contact">
              <button className="bg-white text-accent p-2 text-xl font-medium absolute bottom-3 right-3 left-3 rounded-md">
                Get Now
              </button>
            </Link>
          </div>
        </div>
      </div>

      <div className="flex rounded-md justify-between items-center my-10 lg:mx-36 bg-accent">
        <div>
          <p className="text-white font-bold rounded-md ml-6 mt-9 text-2xl">
            We can make a Prototype.
          </p>
          <p className="text-white text-sm mt-3 ml-6 pb-8">
            that can be tested in Indian weather condition.
          </p>
        </div>
        <div className="lg:mx-20 mx-5">
          <Link to="/contact">
            <button className="text-accent bg-white px-3 py-1 rounded-sm font-medium">
              Get a Quote
            </button>
          </Link>
        </div>
      </div>
    </>
  );
};

export default BusinessCard;
