import React from "react";
import MemberAlgo from "../../assets/Vector/B_ProjectMember.png";

const ProjectImplementation = () => {
  return (
    <>
      <div className="flex justify-center items-center flex-col my-20">
        <p className="text-2xl font-medium mb-3">
          Project Implementation Member
        </p>
        <span className="w-20 bg-accent p-0.5"></span>
        <img src={MemberAlgo} alt="" className="lg:w-8/12 w-full mt-20" />
      </div>
    </>
  );
};

export default ProjectImplementation;
