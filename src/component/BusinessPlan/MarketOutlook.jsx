import React from "react";
import Graph from "../../assets/Vector/B_market_outlook.png";

const MarketOutlook = () => {
  return (
    <>
      <div className="flex justify-center items-center flex-col my-20">
        <p className="text-2xl font-medium mb-3">
          Market Outlook of LiB Batteries
        </p>
        <span className="w-20 bg-accent p-0.5"></span>
        <img src={Graph} alt="" className="w-8/12 mt-20" />
      </div>
    </>
  );
};

export default MarketOutlook;
