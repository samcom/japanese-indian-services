import React from 'react'
import plant from "../../assets/bg/f_plant.png"
import Dholera from "../../assets/bg/f_Dholera.png"
import india from "../../assets/bg/f_india.png"

const FacilityCard = () => {
    return (
        <div className='lg:p-40 p-5 flex align-center justify-center'>
            <div className='grid lg:grid-cols-2 grid-cols-1 gap-10'>
                <div className='bg-accent lg:p-10 p-5 py-10 rounded-md drop-shadow-md '>
                    <p className='text-2xl text-white mt-5'>Mass production factory 25,000㎡ Scheduled to open in 202</p>
                    <ul className='mt-5'>
                        <li className='text-white lg:mx-10 mx-5 mt-2 list-disc'>Electrode manufacturing department</li>
                        <li className='text-white lg:mx-10 mx-5 list-disc'>Cell manufacturing department</li>
                        <li className='text-white lg:mx-10 mx-5 list-disc '>Charge/Discharge facility</li>
                        <li className='text-white lg:mx-10 mx-5 list-disc'>Electrode manufacturing department</li>
                        <li className='text-white lg:mx-10 mx-5 mb-5 list-disc'>Lab for research</li>
                    </ul>
                </div>
                <div className='bg-accent lg:p-16 p-10 lg:px-20 px-10 rounded'>
                    <div className='h-full w-full bg-white p-5 flex flex-col justify-center items-center rounded'>
                        <p className='text-3xl text-center text-accent font-bold'>Location:</p>
                        <p className='text-2xl text-center text-accent font-medium'>Dholera, Gujarat Area:<br></br> 25,000 square mete</p>
                    </div>
                </div>
                <div className="">
                    <img src={plant} className='w-full h-full object-cover rounded-md drop-shadow-md' alt=''></img>
                </div>
                <div className=''>
                    <img src={Dholera} className="w-full h-full object-cover  rounded-md drop-shadow-md" alt=''></img>
                </div>
                <div className='lg:col-span-2'>
                    <img src={india} className="w-full h-full object-cover  rounded-md drop-shadow-md" alt=''></img>
                </div>
            </div>
        </div>
    )
}

export default FacilityCard