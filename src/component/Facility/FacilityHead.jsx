import React from 'react'

const FacilityHead = () => {
  return (
    <div>
        <div className="flex justify-center items-center bg-accent h-96">
        <p className='text-5xl font-medium uppercase mt-20 text-white'>Facility</p>
       </div>        
    </div>
    
  )
}

export default FacilityHead 