import React from "react";
import { MdEmail } from "react-icons/md";
import { GoLocation } from "react-icons/go";

const ContactUs = () => {
  return (
    <>
      <div className="flex justify-center items-center bg-accent h-96 text-white">
        <p className="text-5xl font-medium uppercase mt-20 ">Contact us</p>
      </div>

      <div className="flex justify-center items-center my-20">
        <div className="grid lg:grid-cols-2 grid-cols-1 gap-10">
          <div className="p-10 overflow-hidden">
            <p className="text-accent font-bold text-xl">
              We would love to hear from you
            </p>
            <div className="flex items-center">
              <div>
                <MdEmail className="h-10 my-10 w-10 p-2 text-white rounded-full bg-accent" />
              </div>
              <div className="text-accent ml-3">
                <p className="font-medium mb-2">Email Us</p>
                <p className="">devashish.mukherji@jismedic.com</p>
              </div>
            </div>
            <div className="flex items-center">
              <div>
                <GoLocation className="h-10 my-5 w-10 p-2 text-white rounded-full bg-accent" />
              </div>
              <div className="text-accent ml-3 lg:w-9/12">
                <p className="font-medium mb-2">Visit Office</p>
                <p className="">
                  5-303, 1-362, KIta-Ku, Uetake-cho Saitama-City, Japan 3310813
                </p>
              </div>
            </div>
          </div>
          <div className="p-10">
            <div className="mb-3">
              <p className="text-accent text-sm mb-2">Your Name (required)</p>
              <input type="text" className="border-2 border-accent rounded lg:w-1/2 w-full" />
            </div>
            <div className="my-3">
              <p className="text-accent text-sm mb-2">Your Email (required)</p>
              <input type="text" className="border-2 border-accent rounded lg:w-1/2 w-full" />
            </div>
            <div className="my-3">
              <p className="text-accent text-sm mb-2">Subject</p>
              <input type="text" className="border-2 border-accent rounded lg:w-1/2 w-full" />
            </div>
            <div className="my-3">
              <p className="text-accent text-sm mb-2">Your Message</p>
              <textarea
                cols="23"
                rows="5"
                className="border-2 border-accent rounded lg:w-1/2 w-full"
              ></textarea>
            </div>
            <button className="bg-accent text-white rounded p-2 text-sm px-4">
              Get a free quote
            </button>
          </div>
        </div>
      </div>
    </>
  );
};

export default ContactUs;
