import React, { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import logo from "../assets/logo/logo.png";
import { BiMenuAltRight } from "react-icons/bi";
import { TiTimes } from "react-icons/ti";

const Navbar = () => {
  const [nav, setNav] = useState(false);
  const [button, setButton] = useState(false);

  useEffect(() => {
    document.addEventListener("scroll", () => {
      window.scrollY > 100 ? setNav(true) : setNav(false);
    });
  }, []);


  return (
    <div
      className={`px-10 py-10 flex justify-between items-center fixed w-full z-50  ${
        nav ? "bg-accent py-5 transition-colors delay-300 ease-in-out" : ""
      }`}
    >
      {/* Laptop Navbar */}
      <div>
        <img src={logo} alt="" className="w-32 bg-white rounded" />
      </div>
      <div className="lg:flex items-center hidden">
        <ul className="flex">
          <NavLink
            to="/"
            className={({ isActive }) =>
              isActive ? "text-orange-400" : "text-white"
            }
          >
            <li>Home</li>
          </NavLink>
          <NavLink
            to="/overview"
            className={({ isActive }) =>
              isActive ? "text-orange-400" : "text-white"
            }
          >
            <li>Overview</li>
          </NavLink>
          <NavLink
            to="/businessPlan"
            className={({ isActive }) =>
              isActive ? "text-orange-400" : "text-white"
            }
          >
            <li>Business Plan</li>
          </NavLink>
          <NavLink
            to="/facility"
            className={({ isActive }) =>
              isActive ? "text-orange-400" : "text-white"
            }
          >
            <li>Facility</li>
          </NavLink>
          <NavLink
            to="/researchDevelopement"
            className={({ isActive }) =>
              isActive ? "text-orange-400" : "text-white"
            }
          >
            <li>Research Developement</li>
          </NavLink>
          <NavLink
            to="/employment"
            className={({ isActive }) =>
              isActive ? "text-orange-400" : "text-white"
            }
          >
            <li>Employment</li>
          </NavLink>
          <NavLink
            to="/contact"
            className={({ isActive }) =>
              isActive ? "text-orange-400" : "text-white"
            }
          >
            <li>Contact</li>
          </NavLink>
        </ul>
      </div>

      {/* Mobile Navbar */}
      <button
        className="xs:block p-0 text-3xl absolute right-5 lg:hidden text-white"
        onClick={() => setButton(!button)}
      >
        {!button ? <BiMenuAltRight /> : <TiTimes />}
      </button>
      <div className={`nav lg:hidden ${button ? "block" : "hidden"}`}>
        <div
          className={`absolute left-0 right-0 top-32 ${nav ? "top-24" : ""}`}
        >
          <ul className="bg-accent  rounded-lg p-3">
            <NavLink
              to="/"
              className={({ isActive }) =>
                isActive ? "text-orange-400" : "text-white"
              }
              onClick={() => {
                setButton(!button);
              }}
            >
              <li className="mb-2 border-b border-[#33333333] p-2">Home</li>
            </NavLink>
            <NavLink
              to="/overview"
              className={({ isActive }) =>
                isActive ? "text-orange-400" : "text-white"
              }
              onClick={() => {
                setButton(!button);
              }}
            >
              <li className="mb-2 border-b border-[#33333333] p-2">Overview</li>
            </NavLink>
            <NavLink
              to="/businessPlan"
              className={({ isActive }) =>
                isActive ? "text-orange-400" : "text-white"
              }
              onClick={() => {
                setButton(!button);
              }}
            >
              <li className="mb-2 border-b border-[#33333333] p-2">
                Business Plan
              </li>
            </NavLink>
            <NavLink
              to="/facility"
              className={({ isActive }) =>
                isActive ? "text-orange-400" : "text-white"
              }
              onClick={() => {
                setButton(!button);
              }}
            >
              <li className="mb-2 border-b border-[#33333333] p-2">Facility</li>
            </NavLink>
            <NavLink
              to="/researchDevelopement"
              className={({ isActive }) =>
                isActive ? "text-orange-400" : "text-white"
              }
              onClick={() => {
                setButton(!button);
              }}
            >
              <li className="mb-2 border-b border-[#33333333] p-2">
                Research Developement
              </li>
            </NavLink>
            <NavLink
              to="/employment"
              className={({ isActive }) =>
                isActive ? "text-orange-400" : "text-white"
              }
              onClick={() => {
                setButton(!button);
              }}
            >
              <li className="mb-2 border-b border-[#33333333] p-2">
                Employment
              </li>
            </NavLink>
            <NavLink
              to="/contact"
              className={({ isActive }) =>
                isActive ? "text-orange-400" : "text-white"
              }
              onClick={() => {
                setButton(!button);
              }}
            >
              <li className="mb-2 border-b border-[#33333333] p-2">Contact</li>
            </NavLink>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
